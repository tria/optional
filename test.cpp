#define CATCH_CONFIG_MAIN

#include <vector>

#include <catch.hpp>
#include <tria/optional.h>

class direct_init_me {
public:
	int sum;
	direct_init_me(int a, int b = 0, int c = 0) {
		sum = a + b + c;
	}
};

template class tria::optional<int>;
template class tria::optional<direct_init_me>;

TEST_CASE( "tria::optional", "[tria][optional]" ) {
	SECTION("Constructors") {
		SECTION("Default constructor") {
			tria::optional<int> opt;
			const tria::optional<int> opt_c;
			REQUIRE_FALSE(opt.has_value());
			REQUIRE_THROWS_AS(opt.value(), std::bad_alloc);
			REQUIRE_THROWS_AS(opt_c.value(), std::bad_alloc);
			tria::optional<int> null_opt(tria::nullopt);
			REQUIRE_FALSE(null_opt.has_value());
		}
		SECTION("Forwarding constructor") {
			tria::optional<int> opt(42);
			REQUIRE(opt.has_value());
			REQUIRE(opt.value() == 42);
		}
		SECTION("Copy constructor") {
			const tria::optional<int> opt(42);
			tria::optional<int> opt_copy(opt);
			REQUIRE(opt.has_value());
			REQUIRE(opt_copy.has_value());
			REQUIRE(opt.value() == 42);
			REQUIRE(opt_copy.value() == 42);
		}
		SECTION("Move constructor") {
			tria::optional<int> opt(42);
			tria::optional<int> opt_copy(std::move(opt));
			REQUIRE_FALSE(opt.has_value());
			REQUIRE(opt_copy.has_value());
			REQUIRE(opt_copy.value() == 42);
		}
		SECTION("Converting copy constructor") {
			const tria::optional<int> opt(42);
			tria::optional<double> opt_copy(opt);
			REQUIRE(opt.has_value());
			REQUIRE(opt_copy.has_value());
			REQUIRE(opt.value() == 42);
			REQUIRE(opt_copy.value() == 42);
		}
		SECTION("Converting move constructor") {
			tria::optional<int> opt(42);
			tria::optional<double> opt_copy(std::move(opt));
			REQUIRE_FALSE(opt.has_value());
			REQUIRE(opt_copy.has_value());
			REQUIRE(opt_copy.value() == 42);
		}
		SECTION("In-place constructor") {
			tria::optional<direct_init_me> opt(1,2);
			REQUIRE(opt.has_value());
			REQUIRE(opt.value().sum == 3);
		}
	}
	SECTION("Assings") {
		SECTION("Forwarding assign") {
			tria::optional<int> opt;
			opt = 42;
			REQUIRE(opt.has_value());
			REQUIRE(opt.value() == 42);
			opt = 42.2;
			REQUIRE(opt.has_value());
			REQUIRE(opt.value() == 42);
		}
		SECTION("Nullopt assign") {
			tria::optional<int> opt(42);
			opt = tria::nullopt;
			REQUIRE_FALSE(opt.has_value());
			opt = tria::nullopt;
			REQUIRE_FALSE(opt.has_value());
		}
		SECTION("Copy assign") {
			const tria::optional<int> opt(42);
			const tria::optional<int> opt_empty;
			tria::optional<int> opt_copy;
			opt_copy = opt;
			REQUIRE(opt.has_value());
			REQUIRE(opt_copy.has_value());
			REQUIRE(opt.value() == 42);
			REQUIRE(opt_copy.value() == 42);
			opt_copy = opt_empty;
			REQUIRE_FALSE(opt_copy.has_value());
		}
		SECTION("Move assign") {
			tria::optional<int> opt(42);
			tria::optional<int> opt_empty = {};
			tria::optional<int> opt_copy;
			opt_copy = std::move(opt);
			REQUIRE_FALSE(opt.has_value());
			REQUIRE(opt_copy.has_value());
			opt_copy = std::move(opt_empty);
			REQUIRE_FALSE(opt_copy.has_value());
		}
		SECTION("Converting copy assign") {
			const tria::optional<int> opt(42);
			const tria::optional<int> opt_empty;
			tria::optional<double> opt_copy;
			opt_copy = opt;
			REQUIRE(opt.has_value());
			REQUIRE(opt_copy.has_value());
			REQUIRE(opt.value() == 42);
			REQUIRE(opt_copy.value() == 42);
			opt_copy = opt_empty;
			REQUIRE_FALSE(opt_copy.has_value());
		}
		SECTION("Converting move assign") {
			tria::optional<int> opt(42);
			tria::optional<double> opt_empty;
			tria::optional<double> opt_copy;
			opt_copy = std::move(opt);
			REQUIRE_FALSE(opt.has_value());
			REQUIRE(opt_copy.has_value());
			REQUIRE(opt_copy.value() == 42);
			opt_copy = std::move(opt_empty);
			REQUIRE_FALSE(opt_copy.has_value());
		}
	}
	SECTION("Pointer operators") {
		SECTION("operator->") {
			tria::optional<std::vector<int>> opt(10);
			const auto opt_c(opt);
			REQUIRE(opt->size() == 10);
			REQUIRE(opt_c->size() == 10);
		}
		SECTION("operator*") {
			tria::optional<std::vector<int>> opt(10);
			const auto opt_c(opt);
			REQUIRE((*opt).size() == 10);
			REQUIRE((*opt_c).size() == 10);
		}
	}
	SECTION("Swap") {
		using std::swap;
		tria::optional<int> opt_a(42);
		tria::optional<int> opt_b;
		swap(opt_a, opt_b);
		REQUIRE_FALSE(opt_a.has_value());
		REQUIRE(opt_b.has_value());
	}
	SECTION("Emplace") {
		tria::optional<int> opt;
		opt.emplace(42);
		REQUIRE(opt.value_or(0) == 42);
	}
	SECTION("Conversions and checks") {
		tria::optional<int> opt;
		const tria::optional<int> opt_c(42);
		REQUIRE((bool)opt == 0);
		int a = opt.value_or(1);
		const int b = opt_c.value_or(2);
		opt = 42;
		int c = opt;
		const int d = opt_c;
		REQUIRE(a == 1);
		REQUIRE(b == 42);
		REQUIRE(c == 42);
		REQUIRE(d == 42);
	}
}