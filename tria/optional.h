#ifndef __TRIA_OPTIONAL__
#define __TRIA_OPTIONAL__

#include <memory>
#include <exception>
#include <type_traits>

#include <iostream>

namespace tria {

	struct nullopt_t {
	  enum class _Construct { _Token };
	  explicit constexpr nullopt_t(_Construct) { }
	};

	constexpr nullopt_t nullopt { nullopt_t::_Construct::_Token };

	class __optional_base__ {}; // for easy SFINAE
	template <class T>
	class optional : __optional_base__ {
	private:
		std::unique_ptr<T> _ptr = nullptr;

	public:

		typedef T value_type;

		// default ctor
		optional() noexcept { }
		optional(const tria::nullopt_t & n) noexcept { }
		// fwd ctor
		optional(T && t) {
			_ptr.reset(new T(std::forward<T>(t)));
		}
		// copy ctor
		optional(const optional<T> & o) {
			_ptr.reset(new T(*o.get()));
		}
		// move ctor
		optional(optional<T> && o) {
			_ptr.reset(new T(std::move(*o.get())));
			o.reset();
		}
		// converting copy ctor
		template <class U>
		optional(const optional<U> & o) {
			_ptr.reset(new T((*o.get())));
		}
		// converting move ctor
		template <class U>
		optional(optional<U> && o) {
			_ptr.reset(new T((std::move(*o.get()))));
			o.reset();
		}
		// in-place ctor
		template< class... Args, typename = typename std::enable_if<std::is_constructible<T, Args&&...>::value>::type>
		explicit optional(Args&&... args) {
			_ptr.reset(new T(args...));
		}

		// fwd assign
		template <typename U, typename = typename std::enable_if<std::is_convertible<U, T>::value>::type>
		optional<T> & operator=(U && u) {
			if (has_value()) {
				*_ptr.get() = std::forward<U>(u);
			} else {
				_ptr.reset(new T(std::forward<U>(u)));
			}
			return *this;
		}

		// nullopt assign
		optional<T> & operator=(const tria::nullopt_t & n) {
			if (has_value()) {
				reset();
			}
		}

		// other copy assign
		optional<T> & operator=(const optional<T> & o) {
			if (o.has_value()) {
				_ptr.reset(new T(*o.get()));
			} else {
				reset();
			}
			return *this;
		}

		// other move assign
		optional<T> & operator=(optional<T> && o) {
			if (o.has_value()) {
				_ptr.reset(new T(std::move(*o.get())));
				o.reset();
			} else {
				reset();
			}
			return *this;
		}

		// other copy converting assign
		template <class U>
		optional & operator=(const optional<U> & o) {
			if (o.has_value()) {
				_ptr.reset(new T(*o.get()));
			} else {
				reset();
			}
			return *this;
		}

		// other move converting assign
		template <class U>
		optional & operator=(optional<U> && o) {
			if (o.has_value()) {
				_ptr.reset(new T(std::move(*o.get())));
				o.reset();
			} else {
				reset();
			}
			return *this;
		}

		// dtor
		~optional() {
			delete _ptr.release();
		}

		void reset() {
			if (has_value()) { delete _ptr.release(); }
		}

		T* get() const {
			return _ptr.get();
		}
		T* get() {
			return _ptr.get();
		}
		T* operator->() const {
			return get();
		}
		T* operator->() {
			return get();
		}
		T& operator*() const {
			return *get();
		}
		T& operator*() {
			return *get();
		}

		void swap(optional<T> & o) {
			_ptr.swap(o._ptr);
		}

		template <class... Args>
		T& emplace(Args&&... args) {
			_ptr.reset(new T(args...));
			return value();
		}

		const T& value() const {
			if (!_ptr) {
				throw std::bad_alloc();
			}
			return *_ptr.get();
		}

		T& value() {
			if (!_ptr) {
				throw std::bad_alloc();
			}
			return *_ptr.get();
		}

		const T& value_or(T && t) const {
			return _ptr? *_ptr.get() : t;
		}

		T& value_or(T && t) {
			return _ptr? *_ptr.get() : t;
		}

		bool has_value() const {
			return (bool)_ptr;
		}

		template<typename = typename std::enable_if<!std::is_same<T, bool>::value>::type >
		operator T() const {
			return value();
		}

		template<typename = typename std::enable_if<!std::is_same<T, bool>::value>::type >
		operator T() {
			return value();
		}

		operator bool() const {
			return has_value();
		}

	};

	template <class T>
	void swap(tria::optional<T> & a, tria::optional<T> & b) {
		a.swap(b);
	}
}

#endif