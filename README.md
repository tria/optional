# tria::optional

Drop-in replacement for `std::optional`, less memory usage (8 vs 32 bytes) and C++11 support.

# TODO

Constructors:

 - [x] default (empty)
 - [x] from nullopt
 - [x] forwarding
 - [x] copy from another optional
 - [x] move from another optional
 - [x] copy from another optional with different type
 - [x] move from another optional with different type
 - [x] in-place
 - [ ] in-place (init-list)

Assigns:

 - [x] nullopt
 - [x] forwarding
 - [x] copy from another optional
 - [x] move from another optional
 - [x] copy from another optional with different type
 - [x] move from another optional with different type

Observers:

 - [x] operator->, operator*
 - [x] has_value, operator bool
 - [x] value, value_or, conversion operator to value_type

Modifiers:

 - [x] swap
 - [x] reset
 - [x] emplace

---

 - [x] Tests
 - [ ] Example